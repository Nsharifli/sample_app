require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @user2 = users(:john)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), params: { user: { name: "",
                                              email: "foo@invalid",
                                              password: "foo",
                                              password_confirmation: "bar" } }
    assert_template 'users/edit'
    assert_select "div.alert", text: "The form contains 4 errors."
  end
  
  test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    name = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: { name:  name,
                                              email: email,
                                              password:              "",
                                              password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end

  test "user edits another user info" do
    log_in_as(@user)

    get edit_user_path(@user2)
    assert_equal "Unauthorized", flash[:info]
    assert_redirected_to root_url
  end

  test "user edits own user info" do
    log_in_as(@user)

    get edit_user_path(@user)

    assert_template 'users/edit'

    assert flash.empty?
  end

  test "user updates another user info" do
    log_in_as(@user2)

    patch user_path(@user), params: { user: { name: @user.name,
      email: @user.email } }

    assert_equal "Unauthorized", flash[:info]

    assert_redirected_to root_url
  end

  test "user updates own user info" do
    log_in_as(@user)

    patch user_path(@user), params: { user: { name: @user.name,
      email: @user.email } }
    @user.reload
    assert_redirected_to @user
  end

  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_url(@user)
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), params: { user: { name:  name,
      email: email,
      password:              "",
      password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end

end

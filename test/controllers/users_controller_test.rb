require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user2 = users(:michael)
    @user = users(:john)
  end

  test "should get new" do
    get signup_path

    assert_response :success
    assert_select "title", "Sign up | Ruby on Rails Tutorial Sample App"
  end

  test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch user_path(@user), params: { user: { name: @user.name,
      email: @user.email } }

    assert_not flash.empty?

    assert_redirected_to login_url
  end

  test "should not allow the admin attribute to be edited via the web" do
    log_in_as(@user)
    assert_not @user.admin?
    patch user_path(@user), params: {
                              user: { password: "password",
                                      password_confirmation: "password",
                                      admin: true } }
    assert_not @user.reload.admin?
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@user)
    assert_no_difference 'User.count' do
      delete user_path(@user2)
    end
    assert_redirected_to root_url
  end
end
